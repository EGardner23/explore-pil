Elizabeth Gardner
CSC 355: Open Source Development
16 October 2020
Week 1
	Throughout the week I have been investigating functions of the Pillow library. Over the weekend I
started experimenting with a couple of simple masks, then I moved on to slightly more complex projects
that I have created and added to each day. For example, one of my earlier projects was to convert a
color photo to black and white. One method of doing this is by converting the picture to “L” mode in one
easy command. However, later in the week I became curious about how I could modify the RGB values of
each pixel. Using the getpixel and putpixel functions from the Image module, I was able to convert a
picture to black and white by taking the average of each pixel’s RGB values (I have not tried using a
weighted average as Jakob suggested in class, but I think that a typical arithmetic mean produces a
pretty good result). Even later in the week I found that with a simple modification, the same algorithm
could be used to invert the colors of a picture (something that Nick had mentioned), so I added that
functionality to the program as well.
	Many of my programs were fairly simple experiments of a single Pillow function. The two mask
projects I did last weekend were a basic introduction to composite images. In my painting project, I
wanted to try some of the ImageFilter methods that had been brought up in class. Between suggestions in
class and examples online, I have had no trouble finding ideas for projects besides the explore-halves
and explore-numpy projects we were given. My split and colorize projects began after I came across a
function that interested me in the Pillow documentation and wanted to see how they worked. The project
that I enjoyed the most was the one on toning, which is based on an example that I found online but that
I was able to modify to produce multiple different color-toned images.
	I did encounter some difficulties in the course of my experiments. In multiple cases I have found
that the Pillow documentation on a particular method is unclear; however, this has led me towards other
useful resources such as geeksforgeeks.org, which had an example for nearly every function that I
tested. I had the most difficulty with a program that is supposed to modify a color photograph so that
it is in full color on the left side, then transitions to become fully in black and white on the right
side, as suggested in class earlier this week. I was unable to find any resources to help me with this
process, so this program remains unfinished. The primary thing that I have learned is that the Pillow
library has many, many image-altering functions, and I have only had a chance to work with a very
limited number of them. I wish I could have had more time to experiment with Pillow, but considering
the time constraints inherent to the block system, I feel like I have gathered a solid foundation of
knowledge to build on next week.
