__author__ = "Elizabeth Gardner"
__date__ = "16 October 2020"
# CSC 355: Open Source Development
# explore-numpy.py
"""Experiment with colors in a numpy array."""
import numpy as np
from PIL import Image


def main():
    """Produce a color with a numpy array."""
    print("numpy")
    # picture = Image.open("images/flowers-wall.jpg")
    #
    # # It is more convenient to work with smaller images.
    # # Here, we create a new image whose width and height
    # # are half of the width and height of the original image.
    # picture = picture.reduce(2)
    #
    # d = np.asarray(picture)
    # # We cannot change the original image data,
    # # but we can change a copy of the data.
    # data = d.copy()
    #
    # # Warning! height precedes width in a NumPy array,
    # # but width precedes height in an Image
    # height, width, depth = data.shape
    #
    # # Take a slice of the array.
    # # This slice is a rectangle at the center of the array.
    # # It contains all elements of the original array except
    # # those elements in the top 1/4, the bottom 1/4, the left-most
    # # 1/4, and the right-most 1/4.
    # slice = data[(height//4):(3*height//4), (width//4):(3*width//4)]
    #
    # # Divide the red, green, and blue components of the elements in
    # # the slice by 2.
    # # This will have the effect of darkening the pixels.
    # data[(height//4):(3*height//4), (width//4):(3*width//4)] = slice // 2
    #
    # # Create a new Image from the changed array.
    # picture = Image.fromarray(data)
    #
    # print(f'(The photograph is {width} x {height} pixels.')
    #
    # picture.show()

    # I just wanted to do a little experimenting with numpy arrays apart from just using them to modify existing
    # images and came up with this little algorithm to model a color swatch with an array.
    width = 400
    height = 400

    array = np.zeros((width, height, 3))
    # width, height = array.size
    for x in range(0, width // 2):
        for y in range(0, height):
            # r, g, b = array[x, y]
            # r = 0
            # g = 0
            # b = 255
            # # print(r, g, b)

            # colors in order b, g, r
            # The red, green, and blue components of the color are side by side rather than overlapping,
            # so the color orange produced here is made up of green and red stripes.
            array[x, y] = 0, 165, 255

    array_pic = Image.fromarray(array, "RGB")
    array_pic.show()


if __name__ == "__main__":
    main()
