__author__ = "Elizabeth Gardner"
__date__ = "13 October 2020"
# CSC 355: Open Source Development
# explore-halves.py
"""Produce a bordered image that is divided by line segments into four quadrants, the top right and bottom left of
which are in black and white."""
import numpy as np
from PIL import Image
from PIL import ImageDraw
from PIL import ImageOps


def main():
    """Produce the image's black and white effect, border, and dividing lines."""
    # read an image from a file
    picture = Image.open(r"C:\Users\eliza\Pictures\tulips.jpg")

    # produce a smaller version of the picture
    original_picture = picture.reduce(2)

    print("original_picture mode = ", original_picture.mode)
    print("original_picture size = ", original_picture.size)

    # produce a black and white image from the color image
    # to allow composition with the color image, make this
    # new image's mode "RGB"
    altered_picture = ImageOps.grayscale(original_picture).convert("RGB")

    # picture_gray = original_picture.convert("RGB")
    # array = np.asarray(picture_gray)
    # pic_array = array.copy()
    #
    # altered_picture = Image.fromarray(pic_array)

    # print("altered_picture mode = ", altered_picture.mode)
    # print("altered_picture mode = ", altered_picture.size)
    
    # create a numpy array with as many rows and columns
    # as the color and black and white images and a depth of one
    width, height = original_picture.size

    # initialize array with zeros
    # specify the type of the elements as 8-bit unsigned integers
    mask_data = np.zeros((height, width), dtype=np.uint8)

    # assign 255 to the all elements in upper left and lower right quadrants of the image

    mask_data[:height // 2, :width // 2] = 255
    mask_data[height // 2:, width // 2:] = 255

    mask = Image.fromarray(mask_data, mode="L")

    print("mask mode = ", mask.mode)
    print("mask size = ", mask.size)

    # put the left half of altered image
    # and the right half of the original image in a single image
    half_and_half = Image.composite(original_picture, altered_picture, mask)

    # draw a vertical and a horizontal line segment to mark
    # the boundary between the four quadrants of the image
    draw = ImageDraw.Draw(half_and_half)

    # specify the endpoints of the vertical line segment
    start = (width // 2, 0)
    end = (width // 2, height)

    # specify the color of the line segment
    fill_color = (0, 0, 0)

    # draw vertical line segment that is 4 pixels wide
    draw.line([start, end], fill=fill_color, width=4)

    # specify the endpoints of the horizontal line segment
    start = (0, height // 2)
    end = (width, height // 2)

    # specify the color of the line segment
    # fill_color = (0, 255, 0)

    # draw horizontal line segment that is 4 pixels wide
    draw.line([start, end], fill=fill_color, width=4)

    # display the image

    img_with_border = ImageOps.expand(half_and_half, border=4, fill='black')

    img_with_border.save("explore-halves.png")
    img_with_border.show()
# end of main()


if __name__ == "__main__":
    main()
